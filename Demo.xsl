<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>My CD Collection</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Title</th>
						<th>Artist</th>
						<th>Price</th>
						<th>Year</th>
					</tr>
					<xsl:for-each select="catalog/cd">
						<xsl:sort select="year"/>
						<xsl:if test="/catalog/cd[1]/title = title">
							<tr>
								<td>AAAAA</td>
								<td>AAAAA</td>
								<td>AAAAA</td>
							</tr>
						</xsl:if>
						<tr>
							<td>
								<xsl:value-of select="title"/>
							</td>
							<xsl:choose>
								<xsl:when test="@special='true'">
									<td bgcolor="#880088">
										<xsl:value-of select="artist"/>
									</td>
								</xsl:when>
								<xsl:when test="price > 10">
									<td bgcolor="#ff00ff">
										<xsl:value-of select="artist"/>
									</td>
								</xsl:when>
								<xsl:when test="price > 9">
									<td bgcolor="#cccccc">
										<xsl:value-of select="artist"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="artist"/>
									</td>
								</xsl:otherwise>
							</xsl:choose>		  

							<td>
								<xsl:value-of select="price"/>
							</td>
							<td>
								<xsl:value-of  select="year" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

