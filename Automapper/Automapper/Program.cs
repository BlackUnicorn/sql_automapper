﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using AutoMapper.QueryableExtensions;

namespace AutomapperConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentUserName = null;
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Person, PersonDto>()
                // works for map only
                //.ConstructUsing(src => new PersonDto("value from mapper"))
                //.ForMember(dest => dest.FullName, opt => { opt.MapFrom<CustomResolver>(); opt.Condition(x => string.IsNullOrEmpty(x.FirstName) == false && string.IsNullOrEmpty(x.LastName) == false); })
                //.ForMember(dest => dest.Title, opt => opt.NullSubstitute("Lord"))
                //.AfterMap((src, dest) => {
                //    foreach (var i in dest.PersonPhone)
                //        i.Person = dest;
                //})

                //doesn't work with map (can't pass parameters)
                .ForMember(m => m.CurrentUserName, opt => opt.MapFrom(src => currentUserName))

                //.ForMember(m => m.CurrentUserName, o => o.MapFrom((src, dest, destMember, resContext) => dest.CurrentUserName = resContext.Items["currentUserName"].ToString() ))
                ;

                cfg.CreateMap<PersonPhone, PersonPhoneDto>()
                .ForMember(dest => dest.PhoneNumberType, opt => opt.MapFrom(src => src.PhoneNumberType.Name));
            });

            using (var context = new AdventureWorks2012Entities())
            {
                //var dto = context.Person.Take(2).ToList().Select(x => Mapper.Map<PersonDto>(x, opt => { opt.Items["Foo"] = "Bar"; opt.Items["currentUserName"] = Environment.UserName; })).ToList();
                //Debug.WriteLine(dto);

                //var dto = context.Person.Take(2).ProjectTo<PersonDto>(new { currentUserName = Environment.UserName }).ToList();
                //Debug.WriteLine(dto);

                var dto = context.Person.UseAsDataSource().For<PersonDto>(new { currentUserName = Environment.UserName }).Where(x => x.LastName.StartsWith("A")).ToList();
                Debug.WriteLine(dto);
            }
        }
    }

    public class CustomResolver : IValueResolver<Person, PersonDto, string>
    {
        public string Resolve(Person source, PersonDto destination, string member, ResolutionContext context)
        {
            return $"{source.FirstName} {source.LastName} {context.Items["Foo"]}";
        }
    }
}
