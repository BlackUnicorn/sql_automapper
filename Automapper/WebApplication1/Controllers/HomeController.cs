﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfigurationProvider mappingConfig;

        public HomeController(IConfigurationProvider mappingConfig)
        {
            this.mappingConfig = mappingConfig;
        }

        public ActionResult Index()
        {
            using (var context = new AdventureWorks2012Entities())
            {
                var dto = context.People.UseAsDataSource(mappingConfig).For<PersonDto>().Where(x => x.LastName.StartsWith("A")).ToList();
                return View(dto);
            }            
        }
    }
}