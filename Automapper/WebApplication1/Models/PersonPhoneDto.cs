﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class PersonPhoneDto
    {
        public int BusinessEntityID { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime ModifiedDate { get; set; }

        public virtual PersonDto Person { get; set; }
        public virtual string PhoneNumberType { get; set; }
    }
}
