﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class PersonDto
    {
        public PersonDto()
            :this("default")
        {
        }
        public PersonDto(string parameter)
        {
            ExtraValue = parameter;
        }
        public int BusinessEntityID { get; set; }
        public string PersonType { get; set; }
        public string ExtraValue { get; set; }
        public bool NameStyle { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public int EmailPromotion { get; set; }
        public string AdditionalContactInfo { get; set; }
        public string Demographics { get; set; }
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IList<PersonPhoneDto> PersonPhones { get; set; } = new List<PersonPhoneDto>();

        // automagic
        public int PersonPhonesCount { get; set; }
        // configured
        public object CurrentUserName { get; set; }

        public string FullName { get; set; }
    }
}
