﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using WebApplication1.Models;

namespace WebApplication1.App_Start
{
    public static class AutomapperConfig
    {
        public static IConfigurationProvider CreateMapping()
        {
            IConfigurationProvider config = new MapperConfiguration(cfg =>
            {
                string currentUserName = null;
                cfg.CreateMap<Person, PersonDto>()
                .ForMember(m => m.CurrentUserName, opt => opt.MapFrom(src => currentUserName))
                .ForMember(m => m.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName));

                cfg.CreateMap<PersonPhone, PersonPhoneDto>()
                .ForMember(dest => dest.PhoneNumberType, opt => opt.MapFrom(src => src.PhoneNumberType.Name));
            });

            return config;
        }
    }
}