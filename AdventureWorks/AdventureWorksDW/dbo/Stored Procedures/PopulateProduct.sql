﻿
CREATE PROCEDURE [dbo].[PopulateProduct]
AS
BEGIN
Insert into [DW].[Product]
           ([ProductID]
           ,[Name]
           ,[ProductNumber]
           ,[Color]
           ,[ListPrice]
           ,[Size]
           ,[SizeUnitMeasureCode]
           ,[WeightUnitMeasureCode]
           ,[Weight]
           ,[ProductLine]
           ,[Class]
           ,[Style]
           ,[ProductCategoryID])
SELECT 
   	   [ProductID]
      ,[Name]
      ,[ProductNumber]
      ,[Color]
      ,[ListPrice]
      ,[Size]
      ,[SizeUnitMeasureCode]
      ,[WeightUnitMeasureCode]
      ,[Weight]
      ,[ProductLine]
      ,[Class]
      ,[Style]
      ,[ProductSubcategoryID]
  FROM [Staging].[Product] p
  WHERE NOT EXISTS (select 1 from [DW].[Product] p2 where p2.[ProductID] = p.[ProductID]);
  -- todo update and delete
END