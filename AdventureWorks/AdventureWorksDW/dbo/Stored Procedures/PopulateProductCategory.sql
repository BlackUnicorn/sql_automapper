﻿CREATE PROCEDURE [dbo].[PopulateProductCategory]
AS
BEGIN
INSERT INTO [DW].[ProductCategory] (CategoryId, Name)
SELECT s.ProductSubcategoryID,
	c.Name + N': ' + s.Name
FROM Staging.ProductCategory c
JOIN Staging.ProductSubcategory s
	ON s.ProductCategoryID = c.ProductCategoryID
WHERE NOT EXISTS (select 1 from [DW].[ProductCategory] c2 where c2.CategoryId = s.ProductSubcategoryId);

-- todo update and delete
END