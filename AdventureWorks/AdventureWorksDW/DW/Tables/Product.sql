﻿CREATE TABLE [DW].[Product] (
    [ProductID]             INT            NOT NULL,
    [Name]                  NVARCHAR (50)  NOT NULL,
    [ProductNumber]         NVARCHAR (25)  NOT NULL,
    [Color]                 NVARCHAR (15)  NULL,
    [ListPrice]             MONEY          NOT NULL,
    [Size]                  NVARCHAR (5)   NULL,
    [SizeUnitMeasureCode]   NCHAR (3)      NULL,
    [WeightUnitMeasureCode] NCHAR (3)      NULL,
    [Weight]                DECIMAL (8, 2) NULL,
    [ProductLine]           NCHAR (2)      NULL,
    [Class]                 NCHAR (2)      NULL,
    [Style]                 NCHAR (2)      NULL,
    [ProductCategoryID]     INT            NULL,
    CONSTRAINT [PK_Product_ProductID] PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [FK_Product_ProductCategory_ProductCategoryID] FOREIGN KEY ([ProductCategoryID]) REFERENCES [DW].[ProductCategory] ([CategoryId])
);

