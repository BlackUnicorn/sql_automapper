﻿CREATE TABLE [DW].[ProductCategory] (
    [CategoryId] INT           NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

