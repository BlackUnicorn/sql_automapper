﻿using System.ServiceModel;
using Contract;

namespace Client
{
    public class Proxy2 : ClientBase<IService2>, IService2
    {
        #region Ctor

        public Proxy2()
            : base("BasicHttpBinding_IService2")
        {
        }

        #endregion Ctor

        public void DoStuff()
        {
            base.Channel.DoStuff();
        }
    }
}