﻿using System.ServiceModel;
using Contract;

namespace Client
{
    public class Proxy : ClientBase<IService1>, IService1
    {
        #region Ctor

        public Proxy()
           : base("BasicHttpBinding_IService1")
        {
        }

        #endregion Ctor

        public string GetData(int value)
        {
            return base.Channel.GetData(value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            return base.Channel.GetDataUsingDataContract(composite);
        }

        public void LongOperation()
        {
            base.Channel.LongOperation();
        }

        public void ThrowOperation()
        {
            base.Channel.ThrowOperation();
        }
    }
}