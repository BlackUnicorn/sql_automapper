﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using Contract;

namespace Client
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var client = new Proxy();
            var a = client.GetData(1);
            Console.WriteLine(a);

            var b = client.GetDataUsingDataContract(new CompositeType { BoolValue = true, StringValue = "Boooo" });
            Console.WriteLine(b);

            var timer = new Stopwatch();
            timer.Restart();

            client.LongOperation();

            Console.WriteLine(timer.ElapsedMilliseconds);

            try
            {
                client.ThrowOperation();
            }
            catch(FaultException<CustomFault> e)
            {
                Console.WriteLine(e.Detail);
            }
                       

            client.Close();

            var client2 = new Proxy2();

            client2.DoStuff();

            client2.Close();
        }
    }
}